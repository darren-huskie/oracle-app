var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();

// Config
var sassSrcPath = './source/scss/**/*.scss';
var sassPublicPath = './public/css/';

// sass
gulp.task('sass', function () {
    return gulp.src(sassSrcPath)
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest(sassPublicPath));
});

// sass:watch
gulp.task('sass:watch', function () {
    gulp.watch(sassSrcPath, ['sass']);
});

gulp.task('default', ['sass', 'sass:watch','browser-sync'], function() {
});

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./public"
        },
        port: 55555,
        files: [
            "public/**/*.css"
        ]
    });
});
