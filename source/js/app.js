
$(function(){

    // Fitvids
    $("#fitVids").fitVids();

    // Nav active classes

    // Variables
    var isNavLatest = window.location.href.indexOf("latest") != -1,
        $navLatestElem = $(".c-primary-nav__item--latest"),
        isNavMyFeed = window.location.href.indexOf("my-feed") != -1,
        $navMyFeedElem = $(".c-primary-nav__item--my-feed"),
        isNavFavourites = window.location.href.indexOf("favourites") != -1,
        $navFavouritesElem = $(".c-primary-nav__item--favourites"),
        isNavCategories = window.location.href.indexOf("categories") != -1,
        $navCategoriesElem = $(".c-primary-nav__item--categories");

    // Latest
    if(isNavLatest == true) {
        $navLatestElem.addClass('is-active');
    }
    // My Feed
    if(isNavMyFeed == true) {
        $navMyFeedElem.addClass('is-active');
    }

    // Favourites
    if(isNavFavourites == true) {
        $navFavouritesElem.addClass('is-active');
    }
    // categories
        if(isNavCategories == true) {
            $navCategoriesElem.addClass('is-active');
    }
    // Highcharts
    if($("#container").length > 0) {
        Highcharts.chart('container', {
            title: {
                text: 'Investments over 2016',
                x: -20 //center
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Pounds Sterling (£ k)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valuePrefix: '£'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'PruFund 1',
                data: [7000, 6900, 9500, 14500, 18200, 21500, 25200, 26500, 29300, 32300, 33900, 39600]
            }, {
                name: 'PruFund 2',
                data: [5000, 7900, 8500, 13700, 14700, 20050, 23700, 24500, 26370, 31000, 31700, 34300]
            }, {
                name: 'PruFund 3',
                data: [3000, 5200, 6700, 10500, 10200, 17500, 24800, 25000, 27200, 30035, 31200, 32180]
            }]
        });
    }
});
